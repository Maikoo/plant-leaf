import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage = TabsPage;
  imgDetectionPlugin: any;
  loadAllImg = 0;
  patternsHolder = [];
  indexes = {};
  limit = 3;
  url = ["target1.jpg", "target2.jpg", "target3.jpg"];

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // statusBar.styleDefault();
      statusBar.overlaysWebView(true);
      statusBar.backgroundColorByHexString('#AARRGGBB');
      splashScreen.hide();
      // this.intialized();
    });
  }

  intialized() {
    this.receivedEvent('deviceready');
    this.imgDetectionPlugin = (<any>window).plugins.ImageDetectionPlugin;
    this.imgDetectionPlugin.startProcessing(true, function(success){console.log('START SUCCESS: ', success);}, function(error){console.log('START ERROR: ',error);});
    
    this.imgDetectionPlugin.isDetecting(function(success){
      console.log('IS DETECTING: ', success);
      let resp = JSON.parse(success);
      console.log(resp.index, "image detected - ", this.indexes[resp.index]);
    }, function(error){console.log(error);});

    this.url.forEach((value, key) => {
      this.imgToBase64("./assets/img/patterns/" + value).then((result: any) => {
        this.patternsHolder.push(result.dataURL);
        this.indexes[this.loadAllImg] = (result.image.path[0].src).substr((result.image.path[0].src).lastIndexOf("/") + 1);
        this.loadAllImg += 1;
        console.log("!!!", this.loadAllImg, this.indexes);
        if(this.loadAllImg <= this.limit){
          console.log("patterns set", this.patternsHolder);
          this.setAllPatterns(this.patternsHolder);
        }
      });
    });
    
    this.imgDetectionPlugin.setDetectionTimeout(2, function(success){console.log('TIME OUT SUCCESS: ', success);}, function(error){console.log('TIME OUT ERROR: ', error);});
  }

  setAllPatterns(patterns) {
    this.imgDetectionPlugin.setPatterns(patterns, function(success){console.log('PATTERN SUCCESS: ',success);}, function(error){console.log('PATTERN ERROR: ',error);});
  }

  imgToBase64(url) {
    return new Promise((resolve, reject) => {
      let canvas = document.createElement("canvas");
      let ctx: any = canvas.getContext("2d");
      let img1 = new Image();
      let dataURL;
  
      img1.crossOrigin = "Anonymous";
      img1.onload = function(image: any) {
        console.log(this);
        console.log(image);
        canvas.width = image.path[0].width;
        canvas.height = image.path[0].height;
        ctx.drawImage(this, 0, 0);
        dataURL = canvas.toDataURL("image/jpeg", 0.8);
        resolve({dataURL, image});
        canvas = null;
      };
      img1.src = url;
    });
  }

  ToDataURL(self) {
    let canvas = document.createElement('canvas');
    let ctx = canvas.getContext('2d');
    let dataURL;
    canvas.height = self.height;
    canvas.width = self.width;
    ctx.drawImage(self, 0, 0);
    dataURL = canvas.toDataURL("image/jpeg", 0.8);
    this.patternsHolder.push(dataURL);
    this.indexes[this.loadAllImg] = self.src.substr(self.src.lastIndexOf("/") + 1);
    this.loadAllImg += 1;
    console.log("!!!", this.loadAllImg, this.indexes);
    if(this.loadAllImg <= this.limit){
      console.log("patterns set", this.patternsHolder);
      this.setAllPatterns(this.patternsHolder);
    }
    canvas = null;
  }

  receivedEvent(id) {
    let parentElement = document.getElementById(id);
    let listeningElement = parentElement.querySelector('.listening');
    let receivedElement = parentElement.querySelector('.received');

    listeningElement.setAttribute('style', 'display:none;');
    receivedElement.setAttribute('style', 'display:block;');

    console.log('Received Event: ' + id);
  }
}
