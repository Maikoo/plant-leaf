import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

@Pipe({
  name: 'searchInfo'
})
export class SearchInfoPipe implements PipeTransform {

  transform(arrayToSearch: any, searchTerm: string): any {
    if (!searchTerm) return arrayToSearch;

    searchTerm = _.lowerCase(searchTerm);

    return _.filter(arrayToSearch, function(item:any) {
      // console.log('item filter',item);
      if (_.lowerCase(item.name).indexOf(searchTerm) >- 1) {
        return true;
      } else if (_.lowerCase(item.$key).indexOf(searchTerm) >- 1) {
        return true;
      // } else if (item.message) {
      //   return lodash.lowerCase(item.message).indexOf(searchTerm) >- 1;
      } else {
        return false;
      }
    });
  }

}
