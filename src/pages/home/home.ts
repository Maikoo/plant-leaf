import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { InfoPage }from'../info/info';
import { StartPage } from '../start/start';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  infoPage = InfoPage;
  startPage = StartPage;

  constructor(public navCtrl: NavController) {

  }

}
